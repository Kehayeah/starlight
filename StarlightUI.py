import sys
import can
import threading
import os
from os.path import abspath, dirname, join


from PySide6.QtCore import QObject, Slot
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtCore import QTimer


global rdsText
rdsText  = "hold"
global source
source = "Place"

def canRead():
    #Define stuff
    global rdsText
    global isSeeking
    global source
    bus = can.Bus(interface='slcan', channel='COM10', receive_own_messages=True)
    for msg in bus:
        id = msg.arbitration_id
        #print (id)
        if id == 677:
            rdsbytearr = msg.data
            if rdsbytearr == b'\x00\x00\x00\x00\x00\x00\x00\x00':
                rdsText = "No RDS Available"
            else:
                rdsText = rdsbytearr.decode()
        elif id == "1A3":
            print ("1A3")
        elif id == 549:
            radioStatusarr = msg.data
            radioStatus = radioStatusarr.hex('#')
            radioSplit = radioStatus.split("#")
            if str(radioSplit[2]) == "10":
                source = "FM-1"
            elif str(radioSplit[2]) == "20":
                source = "FM-2"
            elif str(radioSplit[2]) == "40":
                source = "FM-3"
            elif str(radioSplit[2]) == "50":
                source = "MW"
            

class Bridge(QObject):

    def getRDS(value):
        rds = value
        return

    def setStation(self):
        return rds



if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()

    #Start Reading Thread
    th = threading.Thread(target=canRead)
    th.start()
    
    # Instance of the Python object
    bridge = Bridge()

    # Expose the Python object to QML
    context = engine.rootContext()
    context.setContextProperty("con", bridge)

    # Get the path of the current directory, and then add the name
    # of the QML file, to load it.
    qmlFile = join(dirname(__file__), 'dashUI.qml')
    engine.load(abspath(qmlFile))
    def update_rds():
        engine.rootObjects()[0].setProperty('source', source)
        engine.rootObjects()[0].setProperty('rdsText', rdsText)
        
    timer = QTimer()
    timer.setInterval(1000)  # msecs 1000 = 1 sec
    timer.timeout.connect(update_rds)
    timer.start()
    if not engine.rootObjects():
        sys.exit(-1)

    sys.exit(app.exec_())
