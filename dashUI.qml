import QtQuick 2.14
import QtQuick.Window 2.14

Window {
    width: 1024
    height: 600
    visible: true
    color: "#750273"
    title: qsTr("Car Stuff")
    property string rdsText: "Plc"
    property string source: "Source"

    Rectangle {
        id: rectangle
        x: 10
        y: 9
        width: 1004
        height: 583
        color: "#68ffffff"
        radius: 12

        Rectangle {
            id: rectangle1
            x: 51
            y: 95
            width: 420
            height: 185
            color: "#60ffffff"
            radius: 15
            border.width: 0

            Text {
                id: text1
                x: 185
                y: 8
                width: 51
                height: 19
                text: "Source"
                font.pixelSize: 15
                textFormat: Text.RichText
                minimumPixelSize: 12
            }

            Image {
                id: image
                x: 27
                y: 49
                width: 87
                height: 87
                source: "qrc:/qtquickplugin/images/template_image.png"
                fillMode: Image.PreserveAspectFit
            }

            Text {
                id: text4
                x: 133
                y: 49
                text: rdsText
                font.pixelSize: 22
                textFormat: Text.RichText
            }

            Text {
                id: text5
                x: 133
                y: 80
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">108.00 MHz</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }

            Text {
                id: text6
                x: 133
                y: 113
                text: source
                font.pixelSize: 20
                textFormat: Text.RichText
            }

            Text {
                id: text7
                x: 230
                y: 114
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">1</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }
        }

        Text {
            id: text2
            x: 912
            y: 11
            text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">25/03/2021</span></p></body></html>"
            font.pixelSize: 12
            textFormat: Text.RichText
        }

        Text {
            id: text3
            x: 482
            y: 8
            text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">20:03</span></p></body></html>"
            font.pixelSize: 12
            textFormat: Text.RichText
        }

        Rectangle {
            id: rectangle2
            x: 564
            y: 95
            width: 372
            height: 139
            color: "#61ffffff"
            radius: 15

            Text {
                id: text8
                x: 155
                y: 8
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Trip Info</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }

            Image {
                id: image1
                x: 40
                y: 43
                width: 53
                height: 53
                source: "qrc:/qtquickplugin/images/template_image.png"
                fillMode: Image.PreserveAspectFit
            }

            Image {
                id: image2
                x: 160
                y: 43
                width: 53
                height: 53
                source: "qrc:/qtquickplugin/images/template_image.png"
                fillMode: Image.PreserveAspectFit
            }

            Image {
                id: image3
                x: 280
                y: 43
                width: 53
                height: 53
                source: "qrc:/qtquickplugin/images/template_image.png"
                fillMode: Image.PreserveAspectFit
            }

            Text {
                id: text9
                x: 35
                y: 102
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">180 Km</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }

            Text {
                id: text10
                x: 155
                y: 102
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">180 Km</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }

            Text {
                id: text11
                x: 275
                y: 102
                text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Titillium Web'; font-size:8pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">180 Km</span></p></body></html>"
                font.pixelSize: 12
                textFormat: Text.RichText
            }
        }

        Rectangle {
            id: rectangle3
            x: 187
            y: 519
            width: 645
            height: 29
            color: "#61ffffff"
            radius: 15

            Rectangle {
                id: rectangle4
                x: 0
                y: 0
                width: 330
                height: 29
                color: "#ffffff"
                radius: 14.5
            }

            Text {
                id: text12
                x: 316
                y: 8
                width: 14
                height: 13
                text: qsTr("15")
                font.pixelSize: 12
                fontSizeMode: Text.VerticalFit
                minimumPointSize: 9
                minimumPixelSize: 20
                textFormat: Text.RichText
            }


        }

        Rectangle {
            id: rectangle5
            x: 564
            y: 318
            width: 372
            height: 114
            color: "#59ffffff"
            radius: 15
        }



    }
}



/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}
}
##^##*/
